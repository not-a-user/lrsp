#!/usr/bin/env bash

set -exo pipefail

PREFIX="lrsp"
TIMESTAMP="$(date -u +%Y-%m-%d)"
ARCHIVE="${PREFIX}-${TIMESTAMP}.tar"
TEMP="$(mktemp)"

git archive --prefix "${PREFIX}/" -o "${ARCHIVE}" HEAD
git submodule foreach --recursive \
    "git archive --prefix=${PREFIX}/\${sm_path}/ --output=${TEMP} HEAD && tar --concatenate --file=$(pwd)/${ARCHIVE} ${TEMP}"

rm -f "${TEMP}"

xz -f "${ARCHIVE}"
