/*
 * lrsp/lrsp.c - libretro port of SMS Plus
 *
 * Copyright 2019 Dominik Muth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * resources:
 *
 *     https://github.com/0ldsk00l/smsplus
 *     https://retrocdn.net/images/a/a2/SMS_Plus_v1.3_src.7z (read docs/porting.txt)
 *     https://github.com/mattgodbolt/Miracle/tree/master/docs
 *     https://github.com/libretro/libretro-samples
 *     https://github.com/libretro/RetroArch/blob/master/libretro-common/include/libretro.h
 */

#include "smsplus/core/shared.h"
#include "smsplus/shell/video.h"

#include "RetroArch/libretro-common/include/libretro.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#define NUM_AUDIO_CHANNELS 2
#define FRAMES_PER_SECOND 60
#define SAMPLES_PER_SECOND 48000
#define SAMPLES_PER_FRAME (SAMPLES_PER_SECOND / FRAMES_PER_SECOND)
#define SAMPLE_SIZE (sizeof(**snd.output))

_Static_assert(SAMPLE_SIZE == sizeof(int16_t), "unexpected sample size");

static uint32_t *frame_buf;
static struct retro_log_callback logging;
static retro_log_printf_t log_cb;

static void fallback_log(enum retro_log_level level, const char *fmt, ...)
{
    (void)level;
    va_list va;
    va_start(va, fmt);
    vfprintf(stderr, fmt, va);
    va_end(va);
}

void retro_init(void)
{
    frame_buf = calloc(VIDEO_WIDTH_SMS * VIDEO_HEIGHT_SMS, sizeof(uint32_t));
}

void retro_deinit(void)
{
    free(frame_buf);
    frame_buf = NULL;
}

unsigned retro_api_version(void)
{
    return RETRO_API_VERSION;
}

void retro_set_controller_port_device(unsigned port, unsigned device)
{
    log_cb(RETRO_LOG_INFO, "Plugging device %u into port %u.\n", device, port);
}

void retro_get_system_info(struct retro_system_info *info)
{
    memset(info, 0, sizeof(*info));
    info->library_name     = "LRSP";
    info->library_version  = "v0.0.3";
    info->need_fullpath    = true;
    info->valid_extensions = "sms|gg";
}

static retro_video_refresh_t video_cb;
static retro_audio_sample_t audio_cb;
static retro_audio_sample_batch_t audio_batch_cb;
static retro_environment_t environ_cb;
static retro_input_poll_t input_poll_cb;
static retro_input_state_t input_state_cb;

void retro_get_system_av_info(struct retro_system_av_info *info)
{
    memset(&info->timing, 0, sizeof(info->timing));
    info->timing.fps = FRAMES_PER_SECOND;
    info->timing.sample_rate = SAMPLES_PER_SECOND;

    memset(&info->geometry, 0, sizeof(info->geometry));
    if (CONSOLE_GG == sms.console) {
        info->geometry.base_width   = VIDEO_WIDTH_GG;
        info->geometry.base_height  = VIDEO_HEIGHT_GG;
    } else {
        info->geometry.base_width   = VIDEO_WIDTH_SMS;
        info->geometry.base_height  = VIDEO_HEIGHT_SMS;
    }
    info->geometry.max_width    = info->geometry.base_width;
    info->geometry.max_height   = info->geometry.base_height;
    info->geometry.aspect_ratio = ((float) info->geometry.base_width) / info->geometry.base_height;
}

void retro_set_environment(retro_environment_t cb)
{
    environ_cb = cb;

    bool no_content = false;
    cb(RETRO_ENVIRONMENT_SET_SUPPORT_NO_GAME, &no_content);

    if (cb(RETRO_ENVIRONMENT_GET_LOG_INTERFACE, &logging)) {
        log_cb = logging.log;
    } else {
        log_cb = fallback_log;
    }
}

void retro_set_audio_sample(retro_audio_sample_t cb)
{
    audio_cb = cb;
}

void retro_set_audio_sample_batch(retro_audio_sample_batch_t cb)
{
    audio_batch_cb = cb;
}

void retro_set_input_poll(retro_input_poll_t cb)
{
    input_poll_cb = cb;
}

void retro_set_input_state(retro_input_state_t cb)
{
    input_state_cb = cb;
}

void retro_set_video_refresh(retro_video_refresh_t cb)
{
    video_cb = cb;
}

void retro_reset(void) {}

#define INPUT(port, field, retrokey, smskey) \
    do { \
        if (input_state_cb((port), RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_ ## retrokey)) \
        { \
            input.field |= INPUT_ ## smskey; \
        } else { \
            input.field &= ~INPUT_ ## smskey; \
        } \
    } while (0)

#define INPUT_PAD(port, retrokey, smskey) INPUT(port, pad[(port)], retrokey, smskey)
#define INPUT_SYSTEM(retrokey, smskey) INPUT(0, system, retrokey, smskey)

static void update_input(void)
{
    input_poll_cb();

    for (int port = 0; port < 2; port++) {
        INPUT_PAD(port, UP, UP);
        INPUT_PAD(port, DOWN, DOWN);
        INPUT_PAD(port, LEFT, LEFT);
        INPUT_PAD(port, RIGHT, RIGHT);
        INPUT_PAD(port, B, BUTTON1);
        INPUT_PAD(port, A, BUTTON2);
    }

    if (CONSOLE_GG == sms.console) {
        INPUT_SYSTEM(START, START);
    } else {
        INPUT_SYSTEM(START, PAUSE);
        INPUT_SYSTEM(SELECT, RESET); // TODO: good idea?
    }
}

static void check_variables(void) {}

static void audio_callback(void)
{
    static int16_t interleaved[NUM_AUDIO_CHANNELS * SAMPLES_PER_FRAME];
    for (int i = 0; i < SAMPLES_PER_FRAME; i++) {
        interleaved[i * 2]     = snd.output[1][i];
        interleaved[i * 2 + 1] = snd.output[0][i];
    }
    audio_batch_cb(interleaved, SAMPLES_PER_FRAME);
}

void retro_run(void)
{
    update_input();

    // render_checkered();

    system_frame(0);

    if (CONSOLE_GG == sms.console) {
        video_cb(&frame_buf[24*VIDEO_WIDTH_SMS + 48], VIDEO_WIDTH_GG, VIDEO_HEIGHT_GG, VIDEO_WIDTH_SMS * 4);
    } else {
        video_cb(frame_buf, VIDEO_WIDTH_SMS, VIDEO_HEIGHT_SMS, VIDEO_WIDTH_SMS * 4);
    }

    audio_callback();

    bool updated = false;
    if (environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE_UPDATE, &updated) && updated)
        check_variables();
}

bool retro_load_game(const struct retro_game_info *info)
{
    if (NULL == info) {
        return false;
    }

    // Print Header
    log_cb(RETRO_LOG_INFO, "%s %s\n", APP_NAME, APP_VERSION);

    // Check the type of ROM
    sms.console = strcmp(strrchr(info->path, '.'), ".gg") ? CONSOLE_SMS : CONSOLE_GG;

    // Load ROM
    if(!load_rom((char*) info->path)) {
        log_cb(RETRO_LOG_ERROR, "Error: Failed to load %s.\n", info->path);
        return false;
    }

    log_cb(RETRO_LOG_INFO, "CRC : %08X\n", cart.crc);
    log_cb(RETRO_LOG_INFO, "SHA1: %s\n", cart.sha1);

    // Set parameters for internal bitmap
    bitmap.width = VIDEO_WIDTH_SMS;
    bitmap.height = VIDEO_HEIGHT_SMS;
    bitmap.depth = 32;
    bitmap.granularity = 4;
    bitmap.data = (unsigned char*) frame_buf;
    bitmap.pitch = (bitmap.width * bitmap.granularity);
    bitmap.viewport.w = VIDEO_WIDTH_SMS;
    bitmap.viewport.h = VIDEO_HEIGHT_SMS;
    bitmap.viewport.x = 0x00;
    bitmap.viewport.y = 0x00;

    // Set parameters for internal sound
    snd.fm_which = SND_EMU2413; // TODO: settings.audio_fmtype;
    snd.fps = FPS_NTSC;
    snd.fm_clock = CLOCK_NTSC;
    snd.psg_clock = CLOCK_NTSC;
    snd.sample_rate = SAMPLES_PER_SECOND; // TODO: settings.audio_rate;
    snd.mixer_callback = NULL;

    sms.territory = TERRITORY_DOMESTIC; // TODO: settings.misc_region;
    if (sms.console != CONSOLE_GG) {
        sms.use_fm = 1; // TODO: settings.audio_fm;
    }

    // Initialize all systems and power on
    system_init();
    system_poweron();

    enum retro_pixel_format fmt = RETRO_PIXEL_FORMAT_XRGB8888;
    if (!environ_cb(RETRO_ENVIRONMENT_SET_PIXEL_FORMAT, &fmt))
    {
        log_cb(RETRO_LOG_INFO, "XRGB8888 is not supported.\n");
        return false;
    }

    check_variables();

    return true;
}

void retro_unload_game(void)
{
    system_poweroff();
    system_shutdown();
}

unsigned retro_get_region(void)
{
    return RETRO_REGION_NTSC;
}

bool retro_load_game_special(unsigned type, const struct retro_game_info *info, size_t num)
{
    return false;
}

typedef struct {
    char header[4];
    uint16_t version;
    vdp_t vdp;
    sms_t sms;
    struct {
        uint8_t fcr[4];
        uint8_t sram[0x8000];
    } cart;
    struct {
        Z80_Regs regs;
        int after_EI;
    } z80;
    FM_Context fm;
    SN76489_Context sn76489;
} state_t;

size_t retro_serialize_size(void)
{
    return sizeof(state_t);
}

#define STATE_VERSION_LRSP 0x0102

bool retro_serialize(void *data, size_t size)
{
    // see smsplus/core/state.c

    if (size < sizeof(state_t)) {
        return false;
    }

    state_t *const state = data;

    /* Write header */
    memcpy(state->header, STATE_HEADER, sizeof(state->header));
    state->version = STATE_VERSION_LRSP;

    /* Save VDP context */
    state->vdp = vdp;

    /* Save SMS context */
    state->sms = sms;

    memcpy(state->cart.fcr, cart.fcr, sizeof(state->cart.fcr));
    memcpy(state->cart.sram, cart.sram, sizeof(state->cart.sram));

    /* Save Z80 context */
    state->z80.regs = *Z80_Context;
    state->z80.after_EI = after_EI;

    /* Save YM2413 context */
    state->fm = *(FM_Context*) FM_GetContextPtr();

    /* Save SN76489 context */
    state->sn76489 = *(SN76489_Context*) SN76489_GetContextPtr(0);

    return true;
}

bool retro_unserialize(const void *data, size_t size)
{
    // see smsplus/core/state.c

    if (size < sizeof(state_t)) {
        return false;
    }

    state_t const*const state = data;

    /* Check header */
    if (0 != memcmp(STATE_HEADER, state->header, sizeof(state->header))) {
        return false;
    }
    if (STATE_VERSION_LRSP != state->version) {
        return false;
    }

    /* Initialize everything */
    z80_reset(0);
    z80_set_irq_callback(sms_irq_callback);
    system_reset();
    if (snd.enabled) {
        sound_reset();
    }

    /* Load VDP context */
    vdp = state->vdp;

    /* Load SMS context */
    sms = state->sms;

    memcpy(cart.fcr, state->cart.fcr, sizeof(state->cart.fcr));
    memcpy(cart.sram, state->cart.sram, sizeof(state->cart.sram));

    /* Load Z80 context */
    *Z80_Context = state->z80.regs;
    after_EI = state->z80.after_EI;

    /* Load YM2413 context */
    FM_SetContext((uint8_t*) &state->fm);

    /* Load SN76489 context */
    SN76489_SetContext(0, (uint8_t*) &state->sn76489);

    /* Restore callbacks */
    z80_set_irq_callback(sms_irq_callback);

    for (int i = 0x00; i <= 0x2F; i++) {
        cpu_readmap[i]  = &cart.rom[(i & 0x1F) << 10];
        cpu_writemap[i] = dummy_write;
    }

    for (int i = 0x30; i <= 0x3F; i++) {
        cpu_readmap[i] = &sms.wram[(i & 0x07) << 10];
        cpu_writemap[i] = &sms.wram[(i & 0x07) << 10];
    }

    sms_mapper_w(3, cart.fcr[3]);
    sms_mapper_w(2, cart.fcr[2]);
    sms_mapper_w(1, cart.fcr[1]);
    sms_mapper_w(0, cart.fcr[0]);

    /* Force full pattern cache update */
    bg_list_index = 0x200;
    for (int i = 0; i < 0x200; i++) {
        bg_name_list[i] = i;
        bg_name_dirty[i] = -1;
    }

    /* Restore palette */
    for (int i = 0; i < PALETTE_SIZE; i++) {
        palette_sync(i, 1);
    }

    viewport_check();

    return true;
}

void *retro_get_memory_data(unsigned id)
{
    if (RETRO_MEMORY_SAVE_RAM == id) {
        return cart.sram;
    } else {
        return NULL;
    }
}

size_t retro_get_memory_size(unsigned id)
{
    if (RETRO_MEMORY_SAVE_RAM == id) {
        return sizeof(cart.sram);
    } else {
        return 0;
    }
}

void retro_cheat_reset(void) {}

void retro_cheat_set(unsigned index, bool enabled, const char *code)
{
    (void)index;
    (void)enabled;
    (void)code;
}

// TODO: How to synchronize with retro_get_memory_data?
void system_manage_sram(uint8_t *sram, int slot, int mode) {}
