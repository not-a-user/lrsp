#!/usr/bin/env bash

set -exo pipefail

# update submodules
git submodule init &&
git submodule update

# fix makefiles from templates
cat libretro-samples/build/make/Makefile |
sed 's/ENDIANNESS_DEFINES=/ENDIANNESS_DEFINES=-DLSB_FIRST/g' |
sed 's/TARGET_NAME[[:space:]]*:=[[:space:]]*sample/TARGET_NAME := lrsp/g' |
sed 's/ENDIANNESS_DEFINES[[:space:]]*+=[[:space:]]*-DMSB_FIRST/ENDIANNESS_DEFINES = -DMSB_FIRST/g' |
cat > Makefile

cat libretro-samples/build/make/Makefile.common |
sed 's/SOURCES_C[[:space:]]*:=[[:space:]]*/SOURCES_C := lrsp.c $(wildcard smsplus\/core\/*.c)/g' |
cat > Makefile.common

cp libretro-samples/build/make/link.T link.T
