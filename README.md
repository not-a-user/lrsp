LRSP - libretro port of SMS Plus
================================

This is a libretro core built around  Ryan Danbrook's fork of SMS Plus
(http://0ldsk00l.ca/, https://github.com/0ldsk00l/smsplus).

It consists of a fairly minimal amount of wrapper code, yet is fully
functional.

An eclipse project is contained that allows debugging the core inside
the running RetroArch instance.

Clone with `--recursive`.

Dependencies
------------

All dependencies are provided as git submodules:

- https://github.com/0ldsk00l/smsplus for the SMS Plus emulator core
- https://github.com/libretro/RetroArch for the `libretro.h` header
- https://github.com/libretro/libretro-samples for the Makefile

Building the libretro core
--------------------------

    ./prepare # updates submodules and fixes Makefiles
    make

With older gcc compilers (e.g. RetroPie on Raspbian jessie) use

    CFLAGS=-std=gnu99 make


Optional: Build RetroArch
-------------------------

Fedora:

    sudo dnf install make automake gcc gcc-c++ kernel-devel mesa-libEGL-devel libv4l-devel libxkbcommon-devel mesa-libgbm-devel Cg libCg zlib-devel freetype-devel libxml2-devel ffmpeg-devel SDL2-devel SDL-devel perl-X11-Protocol perl-Net-DBus pulseaudio-libs-devel openal-soft-devel libusb-devel

Ubuntu:

    sudo apt-get -y install build-essential libxkbcommon-dev zlib1g-dev libfreetype6-dev libegl1-mesa-dev libgles2-mesa-dev libgbm-dev nvidia-cg-toolkit nvidia-cg-dev libavcodec-dev libsdl2-dev libsdl-image1.2-dev libxml2-dev yasm

And then

    cd RetroArch
    ./configure
    make

Run the libretro core in RetroArch
----------------------------------

    retroarch -L lrsp_libretro.so /tmp/rom.sms

Input
-----

- Game pads as expected
- Game Gear start = retropad start
- Master System pause = retropad start
- Master System reset = retropad select

Features
--------

- Video and audio
- Master System and Game Gear roms
- Save states and rewind
- Save files

Known bugs
----------

- Save files (`.srm`) are created even if game does not support saving.